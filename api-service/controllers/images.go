package controllers

import (
	"gopkg.in/kataras/iris.v6/adaptors/websocket"
	"github.com/satori/go.uuid"
	"fmt"
	"os"
	"bufio"
	"encoding/base64"
)

const (
	GET_ID            = "id"
	RECEIVE_IMAGE     = "image-rec"
	COMPLETED_PUSH    = "completed"
	SEND_ID_FOR_IMAGE = "image-id"
	SEND_CORRECT_READ = "part-image-read"
	IMAGE_URL         = "image-url"
)

type (
	Image struct {
		Type string `json:"type"`
		Data []byte `json:"data"`
	}

	ImagesServer struct {
		images map[string]*Image
	}

	MessageReceived struct {
		Id   string `json:"id"`
		Type string `json:"type"`
		Body string `json:"body"`
	}

	MessageReceiveCorrect struct {
		Id        string `json:"id"`
		BytesRead int `json:"bytesRead"`
	}
)

func NewImagesServer() *ImagesServer {
	return &ImagesServer{
		map[string]*Image{},
	}
}

func (is *ImagesServer) Connect(c websocket.Connection) {

	c.OnMessage(func(data []byte) {
		message := string(data)
		fmt.Printf("Not message %v", message)
	})

	c.On(GET_ID, func() {
		fmt.Println("Send ID")
		is.SendId(c)
	})

	c.On(RECEIVE_IMAGE, func(data interface{}) {
		obj := data.(map[string]interface{})
		msgRec := MessageReceived{}
		msgRec.Id = obj["id"].(string)
		msgRec.Type = obj["type"].(string)
		msgRec.Body = obj["body"].(string)
		is.ReceiveBytes(c, msgRec)
	})

	c.On(COMPLETED_PUSH, func(id string) {
		is.CloseSend(c, id)
	})

}

func (is *ImagesServer) SendId(c websocket.Connection) {
	id := uuid.NewV4().String()
	is.images[id] = &Image{}
	c.Emit(SEND_ID_FOR_IMAGE, id)
	fmt.Printf("Sending %v \n", id)
}

func (is *ImagesServer) ReceiveBytes(c websocket.Connection, data MessageReceived) {
	if info, ok := is.images[data.Id]; ok {
		bytes, err := base64.RawStdEncoding.DecodeString(data.Body)
		if err != nil {
			fmt.Println("Ups!")
			fmt.Println(err)
		}else{
			is.images[data.Id].Data = append(info.Data, bytes...)
			is.images[data.Id].Type = data.Type
		}
	}
	bytesRead := MessageReceiveCorrect{
		Id:        data.Id,
		BytesRead: len(data.Body),
	}
	c.Emit(SEND_CORRECT_READ, bytesRead)
}

func (is *ImagesServer) CloseSend(c websocket.Connection, id string) {
	if info, ok := is.images[id]; ok {
		img, err := os.Create(fmt.Sprintf("../static/images/%v.%v", id, info.Type))
		if err != nil {
			fmt.Println(err)
			return
		}
		defer img.Close()
		writer := bufio.NewWriter(img)
		_, err = writer.Write(info.Data)
		err = writer.Flush()
		c.Emit(IMAGE_URL, fmt.Sprintf("http://localhost:8080/images/%v.%v", id, info.Type))
	}
}
