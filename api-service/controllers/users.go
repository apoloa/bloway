package controllers

import (
	usersPB "github.com/apoloa/bloway/users-service/protocol"
	"gopkg.in/kataras/iris.v6"
	"github.com/apoloa/bloway/api-service/errors"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/grpc"
	"context"
	"fmt"
)

type UsersController struct {
	client usersPB.UsersClient
}



func NewUsersController() *UsersController {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	conn, err := grpc.Dial("localhost:4000", opts...)
	if err != nil {
		grpclog.Fatalf("fail to dial: %v", err)
	}
	client := usersPB.NewUsersClient(conn)
	return &UsersController{client}
}

func (uc *UsersController) CreateUser(ctx *iris.Context) {
	ctx.Log(iris.DevMode, "Create User")
	user := &usersPB.User{}
	if err := ctx.ReadJSON(user); err != nil {
		ctx.Log(iris.DevMode, err.Error())
		errors.InvalidUser.SendToContext(ctx)
		return
	}
	keys, err := uc.client.RegisterUser(context.Background(), user)
	if err != nil {
		ctx.Log(iris.DevMode, grpc.ErrorDesc(err))
		errors.ConvertErrorGRPCUsersService(err).SendToContext(ctx)
		return
	}
	ctx.JSON(iris.StatusCreated, keys)
}

func (uc *UsersController) getUser(userId int64) (*usersPB.User, error){
	fmt.Printf("Get User %v \n", userId)
	user := &usersPB.IdUser{
		Id: userId,
	}
	userRet, err := uc.client.GetUser(context.Background(), user)
	if err != nil {
		return nil, err
	}
	userRet.Email = ""
	return userRet, nil

}

func (uc *UsersController) GetUser(ctx *iris.Context) {
	ctx.Log(iris.DevMode, "Get User")
	fmt.Println(ctx.ParamInt64("userID"))
	userId, err := ctx.ParamInt64("userID")
	ctx.Log(iris.DevMode, "%v", userId)
	if err != nil {
		ctx.Log(iris.DevMode, err.Error())
		errors.InvalidId.SendToContext(ctx)
		return
	}
	user := &usersPB.IdUser{
		Id: userId,
	}
	userRet, err := uc.client.GetUser(context.Background(), user)
	userRet.Email = ""
	if err != nil {
		ctx.Log(iris.DevMode, grpc.ErrorDesc(err))
		errors.ConvertErrorGRPCUsersService(err).SendToContext(ctx)
		return
	}
	ctx.JSON(iris.StatusCreated, userRet)
}

func (uc *UsersController) LoginUser(ctx *iris.Context) {
	ctx.Log(iris.DevMode, "Login User")
	loginRequest := &usersPB.LoginUserRequest{}
	if err := ctx.ReadJSON(loginRequest); err != nil {
		ctx.Log(iris.DevMode, err.Error())
		errors.InvalidLoginRequest.SendToContext(ctx)
	}
	oauth, err := uc.client.LoginUser(context.Background(), loginRequest)
	if err != nil {
		ctx.Log(iris.DevMode, grpc.ErrorDesc(err))
		errors.ConvertErrorGRPCUsersService(err).SendToContext(ctx)
		return
	}
	ctx.JSON(iris.StatusOK, oauth)
}

func (uc *UsersController) checkToken(token string, ctx *iris.Context) (*usersPB.User, *errors.ErrorAPI) {
	ctx.Log(iris.DevMode, "Check Token User")
	tokenRequest := &usersPB.TokenAccessRequest{
		AccessToken: token,
	}
	user, err := uc.client.CheckToken(context.Background(), tokenRequest)
	if err != nil {
		ctx.Log(iris.DevMode, grpc.ErrorDesc(err))
		errorAPI := errors.ConvertErrorGRPCUsersService(err)
		return nil, &errorAPI
	}
	return user, nil
}
