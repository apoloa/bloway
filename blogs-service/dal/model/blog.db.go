package model

import (
	pb "github.com/apoloa/bloway/blogs-service/protocol"
	"database/sql"
	"github.com/apoloa/bloway/error-manager/errors"
	"time"
)

type BlogDB struct {
	pb.Blog
}

func (bDB BlogDB) Insert(db *sql.DB) error {
	_, err := db.Query("INSERT INTO core.blogs (name, owner, description, photo) VALUES ($1, $2, $3, $4);", bDB.GetName(), bDB.GetOwner(), bDB.GetDescription(), bDB.GetUrlPhoto())
	if err != nil {
		return err
	}
	return nil
}

func GetBlogByName(db *sql.DB, name string) (*BlogDB, error) {
	return scanRow(db.QueryRow("SELECT id, name, owner, description, photo, likes, access_times, create_at FROM core.blogs WHERE name = $1", name))
}

func GetBlogsByOwner(db *sql.DB, owner int64, offset int64, limit int64) ([]BlogDB, *int64, error) {
	rows, err := db.Query("SELECT id, name, owner, description, photo, likes, access_times, create_at FROM core.blogs WHERE owner = $1 OFFSET $2 LIMIT $3", owner, offset, limit)
	countRow := db.QueryRow("SELECT count(id) FROM core.blogs WHERE owner = $1 ", owner)
	if err != nil {
		return nil, nil, err
	}
	return scanRows(rows, countRow)
}

func GetTrendingTropicBlogs(db *sql.DB, offset int64, limit int64) ([]BlogDB, *int64, error) {
	rows, err := db.Query("SELECT id, name, owner, description, photo, likes, access_times, create_at FROM core.blogs b ORDER BY b.likes :: FLOAT / b.access_times :: FLOAT DESC, create_at OFFSET $1 LIMIT $2;", offset, limit)
	countRow := db.QueryRow("SELECT count(id) FROM core.blogs b ORDER BY b.likes :: FLOAT / b.access_times :: FLOAT DESC, create_at")
	if err != nil {
		return nil, nil, err
	}
	return scanRows(rows, countRow)
}

func scanRow(row *sql.Row) (*BlogDB, error) {
	var blogDB BlogDB
	var timeCreateAt time.Time
	err := row.Scan(&blogDB.Id, &blogDB.Name, &blogDB.Owner, &blogDB.Description, &blogDB.UrlPhoto, &blogDB.Likes, &blogDB.Access, &timeCreateAt)
	blogDB.CreateAt = timeCreateAt.Unix()
	switch {
	case err == sql.ErrNoRows:
		return nil, errors.BlogNotFound
	case err != nil:
		return nil, err
	default:
		return &blogDB, nil
	}
}

func countRows(row *sql.Row) (*int64, error) {
	var count int64
	err := row.Scan(&count)
	switch {
	case err == sql.ErrNoRows:
		return nil, errors.BlogNotFound
	case err != nil:
		return nil, err
	default:
		return &count, nil
	}
}

func scanRows(rows *sql.Rows, row *sql.Row) ([]BlogDB, *int64, error) {
	defer rows.Close()
	elements, err := countRows(row)
	if err != nil {
		return nil, nil, err
	}
	blogs := []BlogDB{}
	for rows.Next() {
		var blogDB BlogDB
		var timeCreateAt time.Time
		err := rows.Scan(&blogDB.Id, &blogDB.Name, &blogDB.Owner, &blogDB.Description, &blogDB.UrlPhoto, &blogDB.Likes, &blogDB.Access, &timeCreateAt)
		blogDB.CreateAt = timeCreateAt.Unix()
		if err != nil {
			return blogs, nil, err
		}
		blogs = append(blogs, blogDB)
	}
	return blogs, elements, nil
}
