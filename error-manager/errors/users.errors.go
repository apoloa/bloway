package errors

import "errors"

var UserNotFound = errors.New("User Not Found")
var UsernameAlreadyRegistered = errors.New("UserName Already Registered")
var EmailAlreadyRegistered = errors.New("Email Already Registered")
var PasswordRequired =   errors.New("Password Required")
var EmailRequired =  errors.New("Email Required")
var IncorrectEmailPassword = errors.New("Incorrect Parameters")
var TokenExpired = errors.New("Token Expired")
var InvalidToken = errors.New("Invalid Token")

