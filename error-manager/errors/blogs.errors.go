package errors

import "errors"

var BlogNotFound = errors.New("Blog Not Found")
var BlogNameAlreadyExist = errors.New("Blog name already exists")
var NameRequired = errors.New("Name Required")
var OwnerRequired = errors.New("Owner Required")
var DescriptionRequired = errors.New("Description Required")
var UrlPhotoRequired = errors.New("UrlPhotoRequired")
