package main

import (
	"gopkg.in/mgo.v2"
	"net"
	"fmt"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/grpc"
	"github.com/apoloa/bloway/posts-service/router"
	"github.com/apoloa/bloway/posts-service/dal"
)

const DATABASE = "bloway"

func main() {
	session, err := mgo.Dial("")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	session.SetMode(mgo.Monotonic, true)
	db := session.DB(DATABASE)
	postDal := dal.NewPostsDal(db)
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 4002))
	if err != nil {
		grpclog.Fatalf("Failed to Listen: %v", err)
	}
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	postsRouter := router.NewPostsRouter(postDal)
	postsRouter.RegisterRouter(grpcServer)
	grpcServer.Serve(lis)

}
