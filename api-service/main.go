package main

import (
	"gopkg.in/kataras/iris.v6"
	"gopkg.in/kataras/iris.v6/adaptors/gorillamux"
	"gopkg.in/kataras/iris.v6/adaptors/websocket"
	"gopkg.in/kataras/iris.v6/adaptors/cors"
	"github.com/apoloa/bloway/api-service/errors"
	"github.com/apoloa/bloway/api-service/controllers"
	"os"
)

func main() {
	app := iris.New()
	app.Adapt(iris.DevLogger()) // writes both prod and dev logs to the os.Stdout
	app.Adapt(gorillamux.New()) // uses the gorillamux for routing and reverse routing
	app.Adapt(cors.New(cors.Options{
		AllowedMethods:     []string{"GET", "POST", "OPTIONS", "HEAD"},
		AllowedOrigins:     []string{"*"},
		AllowedHeaders:     []string{"*"},
		ExposedHeaders:     []string{"*"},
	}))
	userController := controllers.NewUsersController()
	blogsController := controllers.NewBlogsController(userController)
	postsController := controllers.NewPostsController(userController)
	imagesController := controllers.NewImagesServer()
	app.OnError(iris.StatusNotFound, func(ctx *iris.Context) {
		errors.PageNotFound.SendToContext(ctx)
	})

	iaInterceptor := func (ctx *iris.Context) {
		f, err := os.Create("/log/request.log")
		if (err != nil){
			ctx.Request.Header.Write(f)
			f.WriteString("|")
			f.WriteString(ctx.Request.Method)
			f.WriteString("|")
			f.WriteString(ctx.Request.Host)
			f.WriteString("|")
			f.WriteString(ctx.Request.Proto)
			f.WriteString("|")
			f.WriteString(ctx.Request.RequestURI)
			f.WriteString("|")
			f.WriteString(ctx.Request.URL.RawQuery)
			f.WriteString("---------------------------")
		}
		defer f.Close()
	}

	v1 := app.Party("/v1", iaInterceptor)
	{
		v1.Post("/user", userController.CreateUser)
		v1.Get("/user/{userID:[0-9]+}", userController.GetUser)
		v1.Post("/user/login", userController.LoginUser)
		v1.Get("/blogs", blogsController.GetMyBlogs)
		v1.Post("/blog", blogsController.CreateBlog)
		v1.Post("/post", postsController.CreatePost)
		v1.Get("/posts/category/{category:[0-9]+}", postsController.GetPostByCategory)
	}

	admin := app.Party("admin.")
	{
		admin.Post("/user", h)
	}

	websockets := websocket.New(websocket.Config{
		Endpoint: "/ws",
	})

	websockets.OnConnection(imagesController.Connect)
	app.Adapt(websockets)
	app.StaticWeb("/images", "../static/images")

	app.Listen(":8080")
}

func h(ctx *iris.Context) {
	ctx.HTML(iris.StatusOK, "<h1>Path<h1/>"+ctx.Path())
}
