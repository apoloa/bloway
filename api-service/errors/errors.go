package errors

import (
	"github.com/apoloa/bloway/error-manager/errors"
	"gopkg.in/kataras/iris.v6"
	"google.golang.org/grpc"
)

var PageNotFound = ErrorAPI{
	Id:         1,
	StatusCode: 404,
	Message:    "Page not found",
}

var UsernameAlreadyRegistered = ErrorAPI{
	Id:         3,
	StatusCode: 409,
	Message:    "Username already registered",
}
var EmailAlreadyRegistered = ErrorAPI{
	Id:         4,
	StatusCode: 409,
	Message:    "Email already registered",
}
var UsersNotFound = ErrorAPI{
	Id:         5,
	StatusCode: 409,
	Message:    "User Not Found",
}
var UsersError = ErrorAPI{
	Id:         6,
	StatusCode: 500,
	Message:    "Problems with the users errors",
}
var InvalidUser = ErrorAPI{
	Id:         2,
	StatusCode: 400,
	Message:    "Invalid User Object",
}
var InvalidLoginRequest = ErrorAPI{
	Id:         7,
	StatusCode: 400,
	Message:    "Invalid Request",
}
var EmailRequired = ErrorAPI{
	Id:         8,
	StatusCode: 400,
	Message:    "Email Required",
}
var PasswordRequired = ErrorAPI{
	Id:         8,
	StatusCode: 400,
	Message:    "Password Required",
}
var IncorrectLoginParameters = ErrorAPI{
	Id:         8,
	StatusCode: 409,
	Message:    "Invalid Login Parameters",
}

var InvalidBlog = ErrorAPI{
	Id:         9,
	StatusCode: 400,
	Message:    "Invalid Blog Object",
}

var BlogNameAlreadyExist = ErrorAPI{
	Id:         10,
	StatusCode: 409,
	Message:    "Blog Already Exists",
}

var BlogNotFound = ErrorAPI{
	Id:         11,
	StatusCode: 404,
	Message:    "Blog Not Found",
}

var NameRequired = ErrorAPI{
	Id:         12,
	StatusCode: 400,
	Message:    "Name Required",
}

var OwnerRequired = ErrorAPI{
	Id:         13,
	StatusCode: 400,
	Message:    "Owner Required",
}

var DescriptionRequired = ErrorAPI{
	Id:         14,
	StatusCode: 400,
	Message:    "Description Required",
}

var UrlPhotoRequired = ErrorAPI{
	Id:         15,
	StatusCode: 400,
	Message:    "UrlPhoto Required",
}

var InvalidId = ErrorAPI{
	Id:         16,
	StatusCode: 400,
	Message:    "Invalid Id",
}
var BlogsError = ErrorAPI{
	Id:         17,
	StatusCode: 500,
	Message:    "Problems with the users errors",
}

type ErrorAPI struct {
	Id         int `json:"id"`
	StatusCode int `json:"statusCode"`
	Message    string  `json:"message"`
}

func (e ErrorAPI) SendToContext(ctx *iris.Context) {
	ctx.JSON(e.StatusCode, e)
}

func ConvertErrorGRPCUsersService(err error) ErrorAPI {
	desc := grpc.ErrorDesc(err)
	switch desc {
	case errors.UsernameAlreadyRegistered.Error():
		return UsernameAlreadyRegistered
	case errors.EmailAlreadyRegistered.Error():
		return EmailAlreadyRegistered
	case errors.UserNotFound.Error():
		return UsersNotFound
	case errors.EmailRequired.Error():
		return EmailRequired
	case errors.PasswordRequired.Error():
		return PasswordRequired
	case errors.IncorrectEmailPassword.Error():
		return IncorrectLoginParameters
	default:
		return UsersError
	}
}

func ConvertErrorGRPCBlogsService(err error) ErrorAPI {
	desc := grpc.ErrorDesc(err)
	switch desc {
	case errors.BlogNotFound.Error():
		return BlogNotFound
	case errors.BlogNameAlreadyExist.Error():
		return BlogNameAlreadyExist
	case errors.NameRequired.Error():
		return NameRequired
	case errors.OwnerRequired.Error():
		return OwnerRequired
	case errors.DescriptionRequired.Error():
		return DescriptionRequired
	case errors.UrlPhotoRequired.Error():
		return UrlPhotoRequired
	default:
		return BlogsError
	}
}
