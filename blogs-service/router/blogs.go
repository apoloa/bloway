package router

import (
	pb "github.com/apoloa/bloway/blogs-service/protocol"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"github.com/apoloa/bloway/blogs-service/dal"
	"log"
)

type BlogsRouter struct {
	blogsDal *dal.BlogsDal
}

func NewBlogsRouter(blogsDal *dal.BlogsDal) *BlogsRouter {
	return &BlogsRouter{blogsDal: blogsDal}
}

func (br BlogsRouter) RegisterBlog(ctx context.Context, blog *pb.Blog ) (*pb.Blog, error){
	log.Printf("Register Blog %v \n", blog.Name)
	return br.blogsDal.CreateBlog(blog)
}
func (br BlogsRouter) UpdateBlog(ctx context.Context, blog *pb.Blog) (*pb.Blog, error){
	log.Printf("Update Blog %v \n", blog.Name)
	return nil, nil
}
func (br BlogsRouter) GetBlogs(ctx context.Context, searchRequest *pb.SearchRequest) (*pb.BlogsObj, error){
	log.Printf("Get Blogs %v \n", searchRequest.Text)
	return nil, nil
}
func (br BlogsRouter) GetBlogByOwner(ctx context.Context, owner *pb.Owner) (*pb.BlogsObj, error){
	log.Printf("Get Blogs by Owner %v \n", owner.Id)
	return br.blogsDal.GetBlogsByOwner(owner)
}

func (br BlogsRouter) GetTrendingTopicBlogs(ctx context.Context, searchRequest *pb.SearchRequest) (*pb.BlogsObj, error){
	log.Println("Get Trending Topic Blogs")
	return br.blogsDal.GetTrendingTropicBlogs(searchRequest)
}

func (br *BlogsRouter) RegisterRouter(server *grpc.Server) {
	pb.RegisterBlogsServer(server, br)
}
