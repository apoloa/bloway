package controllers

import (
	postsPB "github.com/apoloa/bloway/posts-service/protocol"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/grpc"
	"gopkg.in/kataras/iris.v6"
	"golang.org/x/net/context"
	"github.com/apoloa/bloway/api-service/errors"
	"fmt"
	"github.com/apoloa/bloway/users-service/protocol"
)

type PageObject struct {
	Page     int32 `json:"page"`
	Elements int32 `json:"elements"`
	Total    int32 `json:"total"`
	Posts    []PostWithUser `json:"posts"`
}

type PostWithUser struct {
	Post *postsPB.Post `json:"post"`
	User *users.User `json:"user"`
}

type PostsController struct {
	client          postsPB.PostsClient
	usersController *UsersController
}

func NewPostsController(controller *UsersController) *PostsController {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	conn, err := grpc.Dial("localhost:4002", opts...)
	if err != nil {
		grpclog.Fatalf("fail to dial: %v", err)
	}
	client := postsPB.NewPostsClient(conn)
	return &PostsController{client, controller}
}

func (bc *PostsController) CreatePost(ctx *iris.Context) {
	ctx.Log(iris.DevMode, "Create Post")
	token := ctx.RequestHeader("x_access_token")
	ctx.Log(iris.DevMode, token)
	user, errorAPI := bc.usersController.checkToken(token, ctx)
	if errorAPI != nil {
		errorAPI.SendToContext(ctx)
		return
	}
	post := &postsPB.Post{}
	if err := ctx.ReadJSON(post); err != nil {
		ctx.Log(iris.DevMode, err.Error())
		errors.InvalidBlog.SendToContext(ctx)
		return
	}
	post.Owner = user.Id
	postRet, err := bc.client.CreatePost(context.Background(), post)
	if err != nil {
		ctx.Log(iris.DevMode, grpc.ErrorDesc(err))
		errors.ConvertErrorGRPCBlogsService(err).SendToContext(ctx)
		return
	}
	ctx.JSON(iris.StatusCreated, postRet)
}

func (bc *PostsController) GetPostByCategory(ctx *iris.Context) {
	ctx.Log(iris.DevMode, "Get Post By Category")
	category, err := ctx.ParamInt("category")
	if err != nil {
		category = DEFAULT_PAGE
	}
	page, err := ctx.URLParamInt("page")
	if err != nil {
		page = DEFAULT_PAGE
	}
	elements, err := ctx.URLParamInt("elements")
	if err != nil {
		elements = DEFAULT_ELEMENTS
	}
	categoryRequest := &postsPB.CategoryPostsRequest{
		Category: int32(category),
		Page:     int32(page),
		Elements: int32(elements),
	}
	postRet, err := bc.client.GetPostsByCategory(context.Background(), categoryRequest)
	if err != nil {
		ctx.Log(iris.DevMode, grpc.ErrorDesc(err))
		errors.ConvertErrorGRPCBlogsService(err).SendToContext(ctx)
		return
	}
	// Get the users using grpc
	pageObject := &PageObject{
		Page:     postRet.Page,
		Elements: postRet.Elements,
		Total:    postRet.Total,
		Posts:    []PostWithUser{},
	}

	for _, post := range postRet.Post {

		func() {
			fmt.Println("Accessing to The Controller Users")
			user, err := bc.usersController.getUser(post.Owner)
			if err != nil {
				ctx.Log(iris.DevMode, grpc.ErrorDesc(err))

				return
			}
			postWithUser := PostWithUser{
				Post: post,
				User: user,
			}
			pageObject.Posts = append(pageObject.Posts, postWithUser)

		}()
	}

	ctx.Log(iris.DevMode, fmt.Sprintf("%v", len(postRet.Post)))
	ctx.JSON(iris.StatusOK, pageObject)
}
