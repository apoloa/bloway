package oauth2

import (
	"github.com/dgrijalva/jwt-go"
	"time"
	"fmt"
	"github.com/apoloa/bloway/error-manager/errors"
	"strconv"
)

var secret = []byte("alkjsdflñkajsñldfkjasñlkfj")

const TimeAccessToken = time.Hour
const timeRefreshToken = time.Hour * 720

func GenerateAccessToken(id int64, ) (string, error) {
	return generateToken(id, TimeAccessToken)
}

func GenerateRefreshToken(id int64) (string, error) {
	return generateToken(id, timeRefreshToken)
}

func CheckAccessToken(token string) (int64, error) {
	tokenObj, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		fmt.Println("token")
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			fmt.Printf("Unexpected signing method: %v\n", token.Header["alg"])
			return 0, errors.InvalidToken
		}
		fmt.Println(secret)
		return secret, nil
	})
	fmt.Println(err)
	if err != nil {
		return 0, err
	}

	if claims, ok := tokenObj.Claims.(jwt.MapClaims); ok && tokenObj.Valid {
		fmt.Println(claims)
		stringDate := claims["date"].(string)
		timeDate, err := strconv.ParseInt(stringDate, 10, 64)
		if err != nil {
			fmt.Println(err)
			return 0, errors.InvalidToken
		}
		dateToken := time.Unix(timeDate, 0)
		fmt.Println(dateToken.Sub(time.Now()))
		if dateToken.Sub(time.Now()) < TimeAccessToken { // Access
			stringId := claims["id"].(string)
			id, err := strconv.ParseInt(stringId, 10, 64)
			if err != nil {
				fmt.Println(err)
				return 0, errors.InvalidToken
			}
			return id, nil

		} else {
			return 0, errors.TokenExpired
		}
		fmt.Println(claims["foo"], claims["nbf"])
	}
	return 0, err
}

func generateToken(id int64, duration time.Duration) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS512, jwt.MapClaims{
		"id":   fmt.Sprintf("%d", id),
		"date": fmt.Sprintf("%d", time.Now().Add(duration).Unix()),
	})
	return token.SignedString(secret)
}
