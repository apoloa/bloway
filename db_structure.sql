create sequence core.blogs_id_seq
;

create sequence core.users_id_seq
;

create table core.blogs
(
	id serial not null
		constraint blogs_pkey
			primary key,
	name varchar not null,
	owner bigint not null,
	description text not null,
	photo varchar not null,
	access_times bigint default 0 not null,
	likes bigint default 0 not null,
	create_at timestamp default ('now'::text)::date not null,
	update_at timestamp default ('now'::text)::date,
	delete_at timestamp
)
;

create unique index blogs_id_uindex
	on core.blogs (id)
;

create unique index blogs_name_uindex
	on core.blogs (name)
;

create table core.users
(
	id bigserial not null
		constraint table_name_pkey
			primary key,
	username varchar not null,
	first_name varchar not null,
	last_name varchar not null,
	email varchar not null,
	password varchar not null,
	photo varchar,
	country varchar,
	city varchar,
	longitude double precision,
	latitude double precision,
	status integer
)
;

create unique index table_name_id_uindex
	on core.users (id)
;

