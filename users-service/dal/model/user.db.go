package model

import (
	pb "github.com/apoloa/bloway/users-service/protocol"
	"database/sql"
	"github.com/apoloa/bloway/error-manager/errors"
	"crypto/sha1"
	"fmt"
)

type UserDB struct {
	pb.User
}

func (uDB UserDB) Insert(db *sql.DB) (*UserDB, error) {

	if _, err := getUserByUsername(db, uDB.GetUserName()); err != errors.UserNotFound {
		if err != nil {
			return nil, err
		}
		return nil, errors.UsernameAlreadyRegistered
	}
	if _, err := getUserByEmail(db, uDB.GetEmail()); err != errors.UserNotFound {
		if err != nil {
			return nil, err
		}
		return nil, errors.EmailAlreadyRegistered
	}
	password := hashFunction(uDB.GetPassword())
	_, err := db.Query("INSERT INTO core.users(username, first_name, last_name, email, password, photo, country, city, longitude, latitude, status) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, 0);",
		uDB.UserName, uDB.FirstName, uDB.LastName, uDB.Email, password, uDB.UrlPhoto, uDB.Country, uDB.City, uDB.Longitude, uDB.Latitude)
	if err != nil {
		return nil, err
	}
	return getUserByEmail(db, uDB.GetEmail())
}

func GetUserByEmailPassword(db *sql.DB, email string, password string) (*UserDB, error) {
	passwordHash := hashFunction(password)
	return scan(db.QueryRow(`SELECT id,
		username,
		first_name,
		last_name,
		email,
		photo,
		country,
		city,
		longitude,
		latitude
	FROM core.users
	WHERE email = $1
	  AND password = $2 `, email, passwordHash))
}

func getUserByUsername(db *sql.DB, username string) (*UserDB, error) {
	return scan(db.QueryRow(`SELECT id,
		username,
		first_name,
		last_name,
		email,
		photo,
		country,
		city,
		longitude,
		latitude
	FROM core.users
	WHERE username = $1`, username))
}

func getUserByEmail(db *sql.DB, email string) (*UserDB, error) {
	return scan(db.QueryRow(`SELECT id,
		username,
		first_name,
		last_name,
		email,
		photo,
		country,
		city,
		longitude,
		latitude
	FROM core.users
	WHERE email = $1`, email))
}

func GetUserById(db *sql.DB, id int64) (*UserDB, error){
	return scan(db.QueryRow(`SELECT id,
		username,
		first_name,
		last_name,
		email,
		photo,
		country,
		city,
		longitude,
		latitude
	FROM core.users
	WHERE id = $1`, id))
}

func scan(row *sql.Row) (*UserDB, error) {
	var userDB UserDB
	err := row.Scan(&userDB.Id, &userDB.UserName, &userDB.FirstName, &userDB.LastName, &userDB.Email, &userDB.UrlPhoto, &userDB.Country, &userDB.City, &userDB.Longitude, &userDB.Latitude)
	switch {
	case err == sql.ErrNoRows:
		return nil, errors.UserNotFound
	case err != nil:
		return nil, err
	default:
		return &userDB, nil
	}
}

func hashFunction(password string) string {
	bPassword := []byte(password)
	hash := sha1.New()
	hash.Write(bPassword)
	return fmt.Sprintf("%x", hash.Sum(nil))
}
