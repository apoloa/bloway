package dal

import (
	pb "github.com/apoloa/bloway/users-service/protocol"
	"github.com/apoloa/bloway/users-service/dal/model"
	"fmt"
	"database/sql"
)

type UserDal struct {
	db *sql.DB
}

func NewUserDal(db *sql.DB) *UserDal {
	return &UserDal{db}
}

func (ud *UserDal) CreateUser(user *pb.User) (*pb.User, error) {
	dbUser := model.UserDB{User: *user}
	newUser, err := dbUser.Insert(ud.db)
	if err != nil {
		return nil, err
	}
	return &newUser.User, nil
}

func (ud *UserDal) GetUserById(idUser *pb.IdUser) (*pb.User, error) {
	fmt.Printf("Get User By Id %v\n", idUser.GetId())
	return getUserById(ud.db, idUser.GetId())
}

func getUserById(db *sql.DB, id int64) (*pb.User, error) {
	user, err := model.GetUserById(db, id)
	if err != nil {
		return nil, err
	}
	return &user.User, nil
}

func getUserByEmailAndPassword(db *sql.DB, email string, password string) (*pb.User, error) {
	user, err := model.GetUserByEmailPassword(db, email, password)
	if err == nil {
		return &user.User, err
	}
	return nil, err
}
