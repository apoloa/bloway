package controllers

import (
	blogsPB "github.com/apoloa/bloway/blogs-service/protocol"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/grpc"
	"gopkg.in/kataras/iris.v6"
	"github.com/apoloa/bloway/api-service/errors"
	"context"
	"fmt"
)

type BlogsController struct {
	client          blogsPB.BlogsClient
	usersController *UsersController
}

const (
	DEFAULT_ELEMENTS = 10
	DEFAULT_PAGE     = 1
)

func NewBlogsController(controller *UsersController) *BlogsController {

	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	conn, err := grpc.Dial("localhost:4001", opts...)
	if err != nil {
		grpclog.Fatalf("fail to dial: %v", err)
	}
	client := blogsPB.NewBlogsClient(conn)
	return &BlogsController{client, controller}
}

func (bc *BlogsController) CreateBlog(ctx *iris.Context) {
	ctx.Log(iris.DevMode, "Create Blog")
	token := ctx.RequestHeader("x_access_token")
	ctx.Log(iris.DevMode, token)
	user, errorAPI := bc.usersController.checkToken(token, ctx)
	if errorAPI != nil {
		errorAPI.SendToContext(ctx)
		return
	}
	blog := &blogsPB.Blog{}
	if err := ctx.ReadJSON(blog); err != nil {
		ctx.Log(iris.DevMode, err.Error())
		errors.InvalidBlog.SendToContext(ctx)
		return
	}
	blog.Owner = user.Id
	blogRet, err := bc.client.RegisterBlog(context.Background(), blog)
	if err != nil {
		ctx.Log(iris.DevMode, grpc.ErrorDesc(err))
		errors.ConvertErrorGRPCBlogsService(err).SendToContext(ctx)
		return
	}
	ctx.JSON(iris.StatusCreated, blogRet)
}

func (bc *BlogsController) GetMyBlogs(ctx *iris.Context) {
	ctx.Log(iris.DevMode, "Get My Blogs")
	token := ctx.RequestHeader("x_access_token")
	page, err := ctx.URLParamInt64("page")
	if err != nil {
		page = DEFAULT_PAGE
	}
	elements, err := ctx.URLParamInt64("elements")
	if err != nil {
		elements = DEFAULT_ELEMENTS
	}
	ctx.Log(iris.DevMode, token)
	user, errorAPI := bc.usersController.checkToken(token, ctx)
	if errorAPI != nil {
		errorAPI.SendToContext(ctx)
		return
	}
	owner := &blogsPB.Owner{
		Id:       user.GetId(),
		Page:     page,
		Elements: elements,
	}
	blogsRet, err := bc.client.GetBlogByOwner(context.Background(), owner)
	if err != nil {
		ctx.Log(iris.DevMode, grpc.ErrorDesc(err))
		errors.ConvertErrorGRPCBlogsService(err).SendToContext(ctx)
		return
	}
	ctx.Log(iris.DevMode, fmt.Sprintf("%v", len(blogsRet.Blogs)))
	ctx.JSON(iris.StatusOK, blogsRet)
}
