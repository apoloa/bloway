package dal

import (
	"database/sql"
	pb "github.com/apoloa/bloway/blogs-service/protocol"
	"github.com/apoloa/bloway/error-manager/errors"
	"github.com/apoloa/bloway/blogs-service/dal/model"
	"log"
)

type BlogsDal struct {
	db *sql.DB
}

func NewBlogsDal(db *sql.DB) *BlogsDal {
	return &BlogsDal{db}
}

func (bd *BlogsDal) CreateBlog(blog *pb.Blog) (*pb.Blog, error) {
	if blog.GetName() == "" {
		return nil, errors.NameRequired
	}
	if blog.GetOwner() == 0 {
		return nil, errors.OwnerRequired
	}
	if blog.GetDescription() == "" {
		return nil, errors.DescriptionRequired
	}
	if blog.GetUrlPhoto() == "" {
		return nil, errors.UrlPhotoRequired
	}
	_, err := model.GetBlogByName(bd.db, blog.GetName())
	if err == nil {
		return nil, errors.BlogNameAlreadyExist
	}
	if err != errors.BlogNotFound {
		return nil, err
	}
	blogDB := model.BlogDB{Blog: *blog}
	err = blogDB.Insert(bd.db)
	if err != nil {
		return nil, err
	}
	blogRet, err := model.GetBlogByName(bd.db, blog.GetName())
	if err != nil {
		return nil, err
	}
	return &blogRet.Blog, nil
}

func (bd *BlogsDal) GetBlogsByOwner(owner *pb.Owner) (*pb.BlogsObj, error) {
	log.Printf("Get Blogs by Owner: %v\n", owner.GetId())
	blogs, numRows, err := model.GetBlogsByOwner(bd.db, owner.Id, (owner.GetPage()-1)*owner.GetElements(), owner.GetElements())
	if err != nil {
		return nil, err
	}
	return convertDBToBlogs(blogs, owner.Page, numRows), nil
}

func (bd *BlogsDal) GetTrendingTropicBlogs(searchRequest *pb.SearchRequest) (*pb.BlogsObj, error) {
	log.Println("Get Trending Topic Blogs")
	blogs, numRows, err := model.GetTrendingTropicBlogs(bd.db, (searchRequest.GetPage()-1)*searchRequest.GetElements(),
		searchRequest.GetElements())
	if err != nil {
		return nil, err
	}
	return convertDBToBlogs(blogs, searchRequest.Page, numRows), nil
}

func convertDBToBlogs(blogs []model.BlogDB, page int64, numberRows *int64) (*pb.BlogsObj) {
	blogsG := []*pb.Blog{}
	for _, blog := range blogs {
		blogG := blog.Blog
		blogsG = append(blogsG, &blogG)
	}
	return &pb.BlogsObj{
		Page:     page,
		Elements: int64(len(blogsG)),
		Total:    *numberRows,
		Blogs:    blogsG,
	}
}
