package main

import (
	"flag"
	"net"
	"fmt"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/grpc"
	"github.com/apoloa/bloway/blogs-service/router"
	"database/sql"
	"github.com/apoloa/bloway/blogs-service/dal"
	_ "github.com/lib/pq"
)

func main(){
	flag.Parse()
	db, err := sql.Open("postgres", "postgresql://bloway:Bloway2016!@localhost:5432/blowaydb?sslmode=disable")
	if err != nil {
		panic(err)
	}
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 4001))
	if err != nil {
		grpclog.Fatalf("Failed to Listen: %v", err)
	}
	blogsDal := dal.NewBlogsDal(db)
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	blogRouter := router.NewBlogsRouter(blogsDal)
	blogRouter.RegisterRouter(grpcServer)
	grpcServer.Serve(lis)
}


