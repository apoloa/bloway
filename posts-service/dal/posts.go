package dal

import (
	"gopkg.in/mgo.v2"
	pb "github.com/apoloa/bloway/posts-service/protocol"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type PostDB struct {
	*pb.Post
	Id    bson.ObjectId `bson:"_id,omitempty"`
	Score float64
}

type PostsDal struct {
	db *mgo.Database
}

const POST_COLLECTION = "post"

func NewPostsDal(db *mgo.Database) *PostsDal {
	return &PostsDal{
		db: db,
	}
}

func (pd *PostsDal) getPostsCollection() *mgo.Collection {
	return pd.db.C(POST_COLLECTION)
}

func (pd *PostsDal) CreatePost(post *pb.Post) (*pb.Post, error) {
	post.Likes = 0
	post.CreateAt = time.Now().Unix()
	post.Access = 0
	postDB := PostDB{
		Id:    bson.NewObjectId(),
		Post:  post,
		Score: 0,
	}
	err := pd.getPostsCollection().Insert(postDB)
	if err != nil {
		return nil, err
	}
	post.Id = postDB.Id.Hex()
	return post, nil
}

func (pd *PostsDal) GetPostsByCategory(page, elements, category int) (*pb.PostsObj, error) {
	iter := pd.getPostsCollection().
		Find(bson.M{"post.category": category}).
		Skip((page - 1) * elements).
		Limit(elements).
		Sort("-post.createat").Iter()
	objRet := &pb.PostsObj{
		Page: int32(page),
	}
	if iter.Err() != nil {
		return nil, iter.Err()
	}
	for {
		var result *PostDB
		ok := iter.Next(&result)
		if ok == false {
			break
		}
		result.Post.Id = result.Id.Hex()
		objRet.Post = append(objRet.Post, result.Post)
	}
	objRet.Elements = int32(len(objRet.Post))
	return objRet, nil
}

func (pd *PostsDal) GetTrendingTopicPostsByCategory(page, elements, category int) (*pb.PostsObj, error) {
	iter := pd.getPostsCollection().
		Find(bson.M{"post.category": category}).
		Skip((page - 1) * elements).
		Limit(elements).
		Sort("-score","-post.createat").Iter()
	objRet := &pb.PostsObj{
		Page: int32(page),
	}
	if iter.Err() != nil {
		return nil, iter.Err()
	}
	for {
		var result *PostDB
		ok := iter.Next(&result)
		if ok == false {
			break
		}
		result.Post.Id = result.Id.Hex()
		objRet.Post = append(objRet.Post, result.Post)
	}
	objRet.Elements = int32(len(objRet.Post))
	return objRet, nil
}