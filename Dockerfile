##############################################
#  Bloway Server
#   Single Server Deploy
##############################################
FROM golang

ADD . /go/src/bloway
ADD ./run_all.sh /go/bin/run_all.sh

RUN apt install protobuf

RUN go get github.com/aws/aws-sdk-go
RUN go get github.com/gin-gonic/gin
RUN go get gopkg.in/mgo.v2
RUN go get gopkg.in/kataras/iris.v6
RUN go get github.com/lib/pq
RUN go get google.golang.org/grpc

RUN go install bloway/api-service
RUN go install bloway/posts-service
RUN go install bloway/blogs-service
RUN go install bloway/users-service

ENTRYPOINT /go/bin/run_all.sh

EXPOSE 8080
