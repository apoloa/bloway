package dal

import (
	pb "github.com/apoloa/bloway/users-service/protocol"
	"github.com/apoloa/bloway/error-manager/errors"
	"github.com/apoloa/bloway/users-service/lib/oauth2"
	"database/sql"
	"fmt"
)

type SessionDal struct {
	db *sql.DB
}

func NewSessionDal (db *sql.DB) *SessionDal{
	return &SessionDal{
		db: db,
	}
}

func (sd SessionDal) Login (request *pb.LoginUserRequest) (*pb.OAUTH2, error) {
	if request.GetPassword() == "" {
		return nil, errors.PasswordRequired
	}
	if request.GetEmail() == "" {
		return nil, errors.EmailRequired
	}

	user, err := getUserByEmailAndPassword(sd.db, request.GetEmail(), request.GetPassword())
	fmt.Println(err)
	switch  {
	case err == errors.UserNotFound:
		return nil, errors.IncorrectEmailPassword
	case err != nil :
		return nil, err
	}
	accessToken, err := oauth2.GenerateAccessToken(user.Id)
	if err != nil {
		return nil, err
	}
	refreshToken, err := oauth2.GenerateRefreshToken(user.Id)
	if err != nil {
		return nil, err
	}
	fmt.Println(err)
	return &pb.OAUTH2{
		AccessToken: accessToken,
		RefreshToken: refreshToken,
		TimeRefresh: int64(oauth2.TimeAccessToken),
		Type: pb.OAUTH2_BEARER,
	}, nil
}

func (sd SessionDal) CheckToken (tokenAccessRequest *pb.TokenAccessRequest) (*pb.User, error) {
	userId, err := oauth2.CheckAccessToken(tokenAccessRequest.GetAccessToken())
	if err != nil {
		return nil, err
	}
	return getUserById(sd.db, userId)
}


