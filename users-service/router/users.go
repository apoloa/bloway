package router

import (
	"golang.org/x/net/context"
	pb "github.com/apoloa/bloway/users-service/protocol"
	"google.golang.org/grpc"
	"github.com/apoloa/bloway/users-service/dal"
)

type UsersRouter struct {
	userDal    *dal.UserDal
	sessionDal *dal.SessionDal
}

func NewUsersRouter(userDal *dal.UserDal, sessionDal *dal.SessionDal) *UsersRouter {
	return &UsersRouter{userDal, sessionDal}
}

func (ur *UsersRouter) RegisterUser(ctx context.Context, user *pb.User) (*pb.OAUTH2, error) {
	_, err := ur.userDal.CreateUser(user)
	if err != nil {
		return nil, err;
	}else{
		loginRequest := pb.LoginUserRequest{
			Email: user.Email,
			Password: user.Password,
		}
		return ur.sessionDal.Login(&loginRequest)
	}
}

func (ur *UsersRouter) UpdateUser(ctx context.Context, user *pb.User) (*pb.User, error) {
	return nil, nil
}

func (ur *UsersRouter) GetUser(ctx context.Context, idUser *pb.IdUser) (*pb.User, error) {
	return ur.userDal.GetUserById(idUser)
}

func (ur *UsersRouter) LoginUser(ctx context.Context, login *pb.LoginUserRequest) (*pb.OAUTH2, error) {
	return ur.sessionDal.Login(login)
}

func (ur *UsersRouter) RefreshToken(ctx context.Context, tokenRefresh *pb.TokenRefreshRequest) (*pb.OAUTH2, error) {
	return nil, nil
}

func (ur *UsersRouter) CheckToken(ctx context.Context, tokenAccess *pb.TokenAccessRequest) (*pb.User, error){
	return ur.sessionDal.CheckToken(tokenAccess)
}

func (ur *UsersRouter) RegisterRouter(server *grpc.Server) {
	pb.RegisterUsersServer(server, ur)
}
