package main

import (
	"flag"
	"net"
	"fmt"
	_ "github.com/lib/pq"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
	"github.com/apoloa/bloway/users-service/router"
	"github.com/apoloa/bloway/users-service/dal"
	"database/sql"
)

func main(){
	flag.Parse()
	db, err := sql.Open("postgres", "postgresql://bloway:Bloway2016!@localhost:5432/blowaydb?sslmode=disable")
	if err != nil {
		panic(err)
	}
	userDal := dal.NewUserDal(db)
	sessionDal := dal.NewSessionDal(db)
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 4000))
	if err != nil {
		grpclog.Fatalf("Failed to Listen: %v", err)
	}
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	usersRouter := router.NewUsersRouter(userDal, sessionDal)
	usersRouter.RegisterRouter(grpcServer)
	grpcServer.Serve(lis)
}
