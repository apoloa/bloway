package router

import (
	"golang.org/x/net/context"
	pb "github.com/apoloa/bloway/posts-service/protocol"
	"github.com/apoloa/bloway/posts-service/dal"
	"google.golang.org/grpc"
)

type PostsRouter struct {
	dal *dal.PostsDal
}

func NewPostsRouter(postsDal *dal.PostsDal) *PostsRouter {
	return &PostsRouter{
		dal: postsDal,
	}
}

func (pr *PostsRouter) GetCategories(ctx context.Context, categoryRequest *pb.CategoryRequest) (*pb.Categories, error) {
	return nil, nil
}
func (pr *PostsRouter) CreatePost(ctx context.Context, post *pb.Post) (*pb.Post, error) {
	return pr.dal.CreatePost(post)
}
func (pr *PostsRouter) GetPostsByBlog(ctx context.Context, blogPostRequest *pb.BlogPostRequest) (*pb.PostsObj, error) {
	return nil, nil
}
func (pr *PostsRouter) SearchPosts(ctx context.Context, searchPostRequest *pb.SearchPostsRequest) (*pb.PostsObj, error) {
	return nil, nil
}
func (pr *PostsRouter) GetPostsByCategory(ctx context.Context, categoryRequest *pb.CategoryPostsRequest) (*pb.PostsObj, error) {
	return pr.dal.GetPostsByCategory(int(categoryRequest.Page), int(categoryRequest.Elements), int(categoryRequest.Category))
}
func (pr *PostsRouter) RegisterRouter(server *grpc.Server) {
	pb.RegisterPostsServer(server, pr)
}
